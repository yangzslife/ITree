package com.sise.itree.common;


import com.sise.itree.core.handle.response.BaseResponse;
import com.sise.itree.model.ControllerRequest;

/**
 * @author idea
 * @date 2019/4/26
 * @Version V1.0
 */
public interface BaseController {

    /**
     * get请求
     *
     * @param controllerRequest
     * @return
     */
    BaseResponse doGet(ControllerRequest controllerRequest);

    /**
     * post请求
     *
     * @param controllerRequest
     * @return
     */
    BaseResponse doPost(ControllerRequest controllerRequest);

}
