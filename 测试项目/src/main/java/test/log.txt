2019-05-02 17:04:47
Full thread dump Java HotSpot(TM) 64-Bit Server VM (25.121-b13 mixed mode):

"DestroyJavaVM" #12 prio=5 os_prio=0 tid=0x00000000027ce000 nid=0xba0 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"Thread-1" #11 prio=5 os_prio=0 tid=0x0000000018843000 nid=0x4240 waiting for monitor entry [0x000000001945f000]
   java.lang.Thread.State: BLOCKED (on object monitor)
	at DeadLock$2.run(DeadLock.java:32)
	- waiting to lock <0x00000000d5fe50c8> (a java.lang.String)
	- locked <0x00000000d5fe50f8> (a java.lang.String)
	at java.lang.Thread.run(Thread.java:745)

   Locked ownable synchronizers:
	- None

"Thread-0" #10 prio=5 os_prio=0 tid=0x000000001883f000 nid=0x39e8 waiting for monitor entry [0x000000001935f000]
   java.lang.Thread.State: BLOCKED (on object monitor)
	at DeadLock$1.run(DeadLock.java:22)
	- waiting to lock <0x00000000d5fe50f8> (a java.lang.String)
	- locked <0x00000000d5fe50c8> (a java.lang.String)
	at java.lang.Thread.run(Thread.java:745)

   Locked ownable synchronizers:
	- None

"Service Thread" #9 daemon prio=9 os_prio=0 tid=0x00000000187ed800 nid=0x910 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"C1 CompilerThread2" #8 daemon prio=9 os_prio=2 tid=0x0000000018771000 nid=0x1930 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"C2 CompilerThread1" #7 daemon prio=9 os_prio=2 tid=0x000000001745e000 nid=0x1e58 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"C2 CompilerThread0" #6 daemon prio=9 os_prio=2 tid=0x000000001745a800 nid=0x35a4 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"Attach Listener" #5 daemon prio=5 os_prio=2 tid=0x0000000017459000 nid=0x29bc waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"Signal Dispatcher" #4 daemon prio=9 os_prio=2 tid=0x000000001740e800 nid=0x2bdc runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
	- None

"Finalizer" #3 daemon prio=8 os_prio=1 tid=0x00000000028ca000 nid=0xf08 in Object.wait() [0x000000001875e000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	- waiting on <0x00000000d5f08ec8> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:143)
	- locked <0x00000000d5f08ec8> (a java.lang.ref.ReferenceQueue$Lock)
	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:164)
	at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:209)

   Locked ownable synchronizers:
	- None

"Reference Handler" #2 daemon prio=10 os_prio=2 tid=0x00000000028c1000 nid=0x4cb8 in Object.wait() [0x000000001865f000]
   java.lang.Thread.State: WAITING (on object monitor)
	at java.lang.Object.wait(Native Method)
	- waiting on <0x00000000d5f06b68> (a java.lang.ref.Reference$Lock)
	at java.lang.Object.wait(Object.java:502)
	at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
	- locked <0x00000000d5f06b68> (a java.lang.ref.Reference$Lock)
	at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)

   Locked ownable synchronizers:
	- None

"VM Thread" os_prio=2 tid=0x00000000173c7000 nid=0x2204 runnable 

"GC task thread#0 (ParallelGC)" os_prio=0 tid=0x00000000027e6800 nid=0x3598 runnable 

"GC task thread#1 (ParallelGC)" os_prio=0 tid=0x00000000027e8000 nid=0x37f4 runnable 

"GC task thread#2 (ParallelGC)" os_prio=0 tid=0x00000000027e9800 nid=0x3764 runnable 

"GC task thread#3 (ParallelGC)" os_prio=0 tid=0x00000000027eb800 nid=0x4b8c runnable 

"VM Periodic Task Thread" os_prio=2 tid=0x000000001882f000 nid=0x1774 waiting on condition 

JNI global references: 6


Found one Java-level deadlock:
=============================
"Thread-1":
  waiting to lock monitor 0x00000000028c8848 (object 0x00000000d5fe50c8, a java.lang.String),
  which is held by "Thread-0"
"Thread-0":
  waiting to lock monitor 0x00000000028c9c38 (object 0x00000000d5fe50f8, a java.lang.String),
  which is held by "Thread-1"

Java stack information for the threads listed above:
===================================================
"Thread-1":
	at DeadLock$2.run(DeadLock.java:32)
	- waiting to lock <0x00000000d5fe50c8> (a java.lang.String)
	- locked <0x00000000d5fe50f8> (a java.lang.String)
	at java.lang.Thread.run(Thread.java:745)
"Thread-0":
	at DeadLock$1.run(DeadLock.java:22)
	- waiting to lock <0x00000000d5fe50f8> (a java.lang.String)
	- locked <0x00000000d5fe50c8> (a java.lang.String)
	at java.lang.Thread.run(Thread.java:745)

Found 1 deadlock.

