public class DeadLock {
    public String locka="a";
    public String lockb="b";

    public static void main(String[] args) {
        new DeadLock().deadLock();

    }

    public void deadLock(){
        Thread t1=new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (locka){
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (lockb){

                    }
                }
            }
        });

        Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (lockb){
                    synchronized (locka){
                        System.out.println("this is b");
                    }
                }
            }
        });
        t1.start();
        t2.start();
    }
}
